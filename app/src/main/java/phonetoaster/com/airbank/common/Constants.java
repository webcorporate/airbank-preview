package phonetoaster.com.airbank.common;

public class Constants {

    public class Global {
        public static final boolean DEBUG_ENABLED = true;
        public static final String TAG_EXCEPTION = "exception";
        public static final String MIME_TYPE = "application/json; charset=utf-8";
        public static final String URL_ENDPOINT = "http://demo0569565.mockable.io/transactions";
        public static final String URL_KEY_INPUT_TEXT_ENCODING = "&ie=";
        public static final String URL_KEY_OUTPUT_TEXT_ENCODING = "&oe=";
        public static final String PREFERENCES_APP_ENABLED = "preferences_app_enabled";
        public static final String PREFERENCES_TRANSLATION_TYPE = "preferences_translation_type";
        public static final String PREFERENCES_TRANSLATION_SOURCE_LANG = "preferences_translation_source_lang";
        public static final String PREFERENCES_TRANSLATION_DEST_LANG = "preferences_translation_dest_lang";
        public static final String KEY_ARRAY_ITEMS = "items";
        public static final String KEY_ARRAY_CONTRA_ACCOUNT = "contraAccount";
        public static final String KEY_ID = "id";
        public static final String KEY_ACCOUNT_NUMBER = "accountNumber";
        public static final String KEY_ACCOUNT_NAME = "accountName";
        public static final String KEY_BANK_CODE = "bankCode";
        public static final String KEY_AMOUNT_IN_ACCOUNT_CURRENCY = "amountInAccountCurrency";
        public static final String KEY_DIRECTION = "direction";
        public static final String VAL_DIRECTION_ICOMING = "INCOMING";
        public static final String VAL_DIRECTION_OUTGOING = "OUTGOING";
        public static final String VAL_DIRECTION_ALL = "";
        public static final int GET = 1;
        public static final int POST = 2;
    }

    public class ExceptionMessage {
        public static final String EXC_CANNOT_CANNOT_PROCESS_REBOOT_RECEIVER = "Cannot process reboot receiver";
    }
}
