package phonetoaster.com.airbank.model;

public class Transaction {

    private Integer id;
    private Integer amountInAccountCurrency;
    private String direction;
    private String accountNumber;
    private String accountName;
    private String bankCode;
    private static Transaction instance;

    public static synchronized Transaction getInstance() {
        if (instance == null) {
            instance = new Transaction();
        }
        return instance;
    }

    public Transaction(Integer id, Integer amountInAccountCurrency, String direction) {
        this.id = id;
        this.amountInAccountCurrency = amountInAccountCurrency;
        this.direction = direction;
        this.accountNumber = null;
        this.accountName = null;
        this.bankCode = null;
    }

    public Transaction(String accountNumber, String accountName, String bankCode) {
        this.id = null;
        this.amountInAccountCurrency = null;
        this.direction = null;
        this.accountNumber = accountNumber;
        this.accountName = accountName;
        this.bankCode = bankCode;
    }

    public Transaction() {
        this.id = null;
        this.amountInAccountCurrency = null;
        this.direction = null;
        this.accountNumber = null;
        this.accountName = null;
        this.bankCode = null;
    }

    public Integer getId() {
        return id;
    }

    public Integer getAmountInAccountCurrency() {
        return amountInAccountCurrency;
    }

    public String getDirection() {
        return direction;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public String getBankCode() {
        return bankCode;
    }



}
