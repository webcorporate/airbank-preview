package phonetoaster.com.airbank.activity;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.orhanobut.logger.Logger;

import java.util.ArrayList;

import phonetoaster.com.airbank.R;
import phonetoaster.com.airbank.common.Constants;
import phonetoaster.com.airbank.controller.TransactionsController;
import phonetoaster.com.airbank.model.Transaction;

public class TransactionsActivity extends MainActivity {

    public ArrayList<Transaction> transactions = new ArrayList<Transaction>();
    public ListAdapter adapter;
    private static TransactionsController mTranactionsController = null;
    private ListView listView;
    private SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);
        initViews();
        initCommon();
    }

    private void initViews() {
        // Lookup the swipe container view
        swipeContainer = (SwipeRefreshLayout) this.findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initCommon();
                swipeContainer.setRefreshing(false);
            }
        });

        listView = ( ListView ) this.findViewById( R.id.list );
        //LIST ITEM CLICK LISTENER REMOVED DUE THE NEED TO PASS OBJECT FROM ADAPTER
    }

    private void initCommon() {
        mTranactionsController = new TransactionsController(this, listView);
        mTranactionsController.execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.transaction_filter_menu, menu);

        MenuItem item = menu.findItem(R.id.spinner);
        Spinner spinner = (Spinner) MenuItemCompat.getActionView(item);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.actions, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        //spinner.setSelection(0, false);
        spinner.setSelected(false);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        mTranactionsController.filterData(Constants.Global.VAL_DIRECTION_ALL);
                        break;
                    case 1:
                        mTranactionsController.filterData(Constants.Global.VAL_DIRECTION_ICOMING);
                        break;
                    case 2:
                        mTranactionsController.filterData(Constants.Global.VAL_DIRECTION_OUTGOING);
                        break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        return true;
    }

    private AdapterView.OnItemSelectedListener mFilterDropdownItemClickListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String selectedItem = parent.getItemAtPosition(position).toString();
            Logger.d(selectedItem);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            Logger.d("onNothingSelected");
        }
    };


}
