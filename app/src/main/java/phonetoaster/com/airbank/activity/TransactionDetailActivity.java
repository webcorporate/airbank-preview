package phonetoaster.com.airbank.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ListView;

import com.orhanobut.logger.Logger;

import phonetoaster.com.airbank.R;
import phonetoaster.com.airbank.common.Constants;
import phonetoaster.com.airbank.controller.TransactionDetailController;
import phonetoaster.com.airbank.controller.TransactionsController;
import phonetoaster.com.airbank.helper.ToastHelper;
import phonetoaster.com.airbank.model.Transaction;

public class TransactionDetailActivity extends MainActivity {

    private TransactionDetailController mTransactionDetailController = null;
    private Transaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_detail);
        initViews();
        initCommon();
    }


    private void initViews() {

    }

    private void initCommon() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            transaction = new Transaction();
            Integer id = extras.getInt(Constants.Global.KEY_ID);
            Integer amountInAccountCurrency = extras.getInt(Constants.Global.KEY_AMOUNT_IN_ACCOUNT_CURRENCY);
            String direction = extras.getString(Constants.Global.KEY_DIRECTION);
            transaction = new Transaction(id, amountInAccountCurrency, direction);
            mTransactionDetailController = new TransactionDetailController(this, transaction);
            mTransactionDetailController.execute();
        } else {
            ToastHelper.showToastMessage(this, "No data passed", true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
