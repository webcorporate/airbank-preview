package phonetoaster.com.airbank.controller;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.orhanobut.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import phonetoaster.com.airbank.R;
import phonetoaster.com.airbank.activity.TransactionDetailActivity;
import phonetoaster.com.airbank.activity.TransactionsActivity;
import phonetoaster.com.airbank.adapter.TransactionsAdapter;
import phonetoaster.com.airbank.common.Constants;
import phonetoaster.com.airbank.helper.JSONParser;
import phonetoaster.com.airbank.helper.ToastHelper;
import phonetoaster.com.airbank.model.Transaction;


public class TransactionDetailController extends AsyncTask<String, Integer, String> {

    private TransactionDetailActivity activity;
    private Transaction transaction;
    private JSONObject data;

    public TransactionDetailController(TransactionDetailActivity context, Transaction transaction) {
        this.activity = context;
        this.transaction = transaction;
    }


    @Override
    protected String doInBackground(String... params) {
        data = JSONParser.getJsonFromUrl(Constants.Global.URL_ENDPOINT + "/" + transaction.getId());
        return "";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Logger.d("onPreExecute");
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Logger.d("onPostExecute");
        Logger.d(data);
        if (data == null) {
            ToastHelper.showToastMessage(activity, "Cannot get data for detail", true);
        } else {
            try {
                JSONObject item = data.getJSONObject(Constants.Global.KEY_ARRAY_CONTRA_ACCOUNT);
                String accountNumber = item.getString(Constants.Global.KEY_ACCOUNT_NUMBER);
                String accountName = item.getString(Constants.Global.KEY_ACCOUNT_NAME);
                String bankCode = item.getString(Constants.Global.KEY_BANK_CODE);

                //TODO ADD SETTERS FOR UPDATE EXISTING OBJECT PASSED VIA INTENT?
                Transaction loadedData = new Transaction(accountNumber, accountName, bankCode);

                ImageView ivDirection = (ImageView) activity.findViewById(R.id.iv_detail_direction);
                TextView tvAmount = (TextView) activity.findViewById(R.id.tv_detail_amount);
                TextView tvDirection = (TextView) activity.findViewById(R.id.tv_detail_direction);
                TextView tvAccountNumber = (TextView) activity.findViewById(R.id.tv_accountNumber);
                TextView tvAccountName = (TextView) activity.findViewById(R.id.tv_accountName);
                TextView tvBankCode = (TextView) activity.findViewById(R.id.tv_bankCode);

                if (transaction.getDirection().equals(Constants.Global.VAL_DIRECTION_ICOMING))
                    ivDirection.setImageResource(R.drawable.arrow_left);
                if (transaction.getDirection().equals(Constants.Global.VAL_DIRECTION_OUTGOING))
                    ivDirection.setImageResource(R.drawable.ic_arrow_right);

                //assert tvAmount != null;
                //tvAmount.setText(transaction.getAmountInAccountCurrency());
                assert tvDirection != null;
                tvDirection.setText(transaction.getDirection());
                assert tvAmount != null;
                tvAmount.setText(transaction.getAmountInAccountCurrency().toString());
                assert tvAccountNumber != null;
                tvAccountNumber.setText(loadedData.getAccountNumber());
                assert tvAccountName != null;
                tvAccountName.setText(loadedData.getAccountName());
                assert tvBankCode != null;
                tvBankCode.setText(loadedData.getBankCode());

            } catch (Exception e) {
                Logger.e(e.getMessage());
                ToastHelper.showToastMessage(activity, "Cannot parse detail transaction data", true);
            }
        }

    }

    //@Override
    protected void onProgressUpdate(Integer... a) {
        Logger.d("onProgressUpdate");
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

}
