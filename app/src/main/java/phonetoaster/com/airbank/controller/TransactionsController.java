package phonetoaster.com.airbank.controller;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.widget.ListView;

import com.orhanobut.logger.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import phonetoaster.com.airbank.adapter.TransactionsAdapter;
import phonetoaster.com.airbank.common.Constants;
import phonetoaster.com.airbank.helper.JSONParser;
import phonetoaster.com.airbank.helper.ToastHelper;
import phonetoaster.com.airbank.model.Transaction;


public class TransactionsController extends AsyncTask<String, Integer, String> {

    private Context context;
    private JSONObject data;
    private static TransactionsController instance = null;
    private ArrayList<Transaction> transactionArrayList;
    private ListView listView;
    private TransactionsAdapter adapter = null;

    public TransactionsController(Context context, ListView listView) {
        this.context = context;
        this.listView = listView;
        transactionArrayList = new ArrayList<>();
    }

    public void filterData(String string) {
            if(adapter != null) {
                adapter.getFilter().filter(string);
            }
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            data = JSONParser.getJsonFromUrl(Constants.Global.URL_ENDPOINT);
            return null;
        } catch (Exception e) {
            Logger.e(e.getMessage());
            return null;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Logger.d("onPreExecute");
        //TODO: SHOW PRELOADER
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Logger.d("onPostExecute");
        try {
            JSONArray items = data.getJSONArray(Constants.Global.KEY_ARRAY_ITEMS);
            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                Integer id = item.getInt(Constants.Global.KEY_ID);
                Integer amount = item.getInt(Constants.Global.KEY_AMOUNT_IN_ACCOUNT_CURRENCY);
                String direction = item.getString(Constants.Global.KEY_DIRECTION);
                Transaction transactionItem = new Transaction(id,amount,direction);
                transactionArrayList.add(transactionItem);
            }

            Logger.d("Size " + transactionArrayList.size());

            //Push values to adapter
            adapter = new TransactionsAdapter(transactionArrayList, this.context);

            Resources res = context.getResources();
            listView.setAdapter(adapter);

        } catch (Exception e) {
            ToastHelper.showToastMessage(context, "Cannot parse data", true);
        }

    }

    //@Override
    protected void onProgressUpdate(Integer ... a){
        Logger.d("onProgressUpdate");
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

}
