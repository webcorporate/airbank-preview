package phonetoaster.com.airbank.helper;

import com.orhanobut.logger.Logger;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import phonetoaster.com.airbank.common.Constants;


/**
 * Created by george on 26/01/17.
 */
public class JSONParser {

    private static final MediaType JSON = MediaType.parse(Constants.Global.MIME_TYPE);

    public static JSONObject getJsonFromUrl(String url) {
        return getJson(url, Constants.Global.GET, null);
    }

    public static JSONObject postJsonFromUrl(String url, String jsonString) {
        return getJson(url, Constants.Global.POST, jsonString);
    }

    private static JSONObject getJson(String url, int method, String jsonStringParams) {
        JSONObject json = null;
        // try parse the string to a JSON object
        try {
            String jsonString = makeServiceCall(url, method, jsonStringParams);
            json = new JSONObject(jsonString);
            return json;
        } catch (Exception e) {
            Logger.e("Cannot parse data");
            return null;
        }
    }

    private static String makeServiceCall(String url, int method, String jsonStringParams) throws IOException {
        OkHttpClient client = new OkHttpClient();
        try {
            Request request;
            if (method == Constants.Global.POST) {
                RequestBody body = RequestBody.create(JSON, jsonStringParams);
                request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
            } else {
                request = new Request.Builder()
                        .url(url)
                        .build();
            }

            Response response = client.newCall(request).execute();
            if(response.isSuccessful())
                return response.body().string();
            else {
                Logger.d(response.code());
                JSONObject jo = new JSONObject();
                jo.put("error_code", response.code());
                return jo.toString();
            }
        } catch (Exception e) {
            Logger.e(e.getMessage());
            return null;
        }
    }
}
