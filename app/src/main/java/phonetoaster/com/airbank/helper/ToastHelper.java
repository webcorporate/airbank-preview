package phonetoaster.com.airbank.helper;

import android.content.Context;
import android.widget.Toast;
import com.orhanobut.logger.Logger;
import phonetoaster.com.airbank.common.Constants;

public class ToastHelper {

    public static void showToastMessage(Context ctx, String message, boolean isDebugMessage) {
        if(Constants.Global.DEBUG_ENABLED && isDebugMessage) {
            Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
            Logger.d(message);
        } else if(!isDebugMessage) {
            Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
        }
    }


}
