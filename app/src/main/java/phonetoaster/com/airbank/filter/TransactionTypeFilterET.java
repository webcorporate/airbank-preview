package phonetoaster.com.airbank.filter;

import android.widget.BaseAdapter;
import android.widget.Filter;
import com.orhanobut.logger.Logger;
import java.util.ArrayList;

import phonetoaster.com.airbank.model.Transaction;


public class TransactionTypeFilterET extends Filter {

    private ArrayList<Transaction> transactionArrayList;
    private BaseAdapter adapter;

    public TransactionTypeFilterET(ArrayList<Transaction> transactionArrayList, BaseAdapter listAdapter) {
        this.transactionArrayList = transactionArrayList;
        this.adapter = listAdapter;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {

        FilterResults filteredResults = new FilterResults();
        if (constraint != null && constraint.length() > 0) {
            ArrayList<Transaction> tempList = new ArrayList<Transaction>();

            // search in values
            for (Transaction transaction : transactionArrayList) {
                if (transaction.getDirection().toLowerCase().contains(constraint.toString().toLowerCase())) {
                    tempList.add(transaction);
                }
            }

            filteredResults.count = tempList.size();
            filteredResults.values = tempList;
        } else {
            filteredResults.count = transactionArrayList.size();
            filteredResults.values = transactionArrayList;
        }
        return filteredResults;
    }

    /**
     * Notify about filtered list to ui
     *
     * @param constraint text
     * @param results    filtered result
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        ArrayList<Transaction> resultArr  = (ArrayList<Transaction>) results.values;
        Logger.e(resultArr.toString());
        adapter.notifyDataSetChanged();
    }
}

