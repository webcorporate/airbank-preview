package phonetoaster.com.airbank.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.orhanobut.logger.Logger;

import java.util.ArrayList;

import phonetoaster.com.airbank.R;
import phonetoaster.com.airbank.activity.TransactionDetailActivity;
import phonetoaster.com.airbank.common.Constants;
import phonetoaster.com.airbank.model.Transaction;

public class TransactionsAdapter extends BaseAdapter implements Filterable {

    private ArrayList<Transaction> originalData;
    private ArrayList<Transaction> filteredData;
    private TransactionTypeFilter transactionTypeFilter = new TransactionTypeFilter();
    private Context activity;


    public TransactionsAdapter(ArrayList<Transaction> data, Context context) {
        this.activity = context;
        this.originalData = data;
        this.filteredData = data;
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View rowView, ViewGroup parent) {
        try {
            //Suing this check we can save the overhead of inflating a new view for each row.
            //This will make our listview faster.
            if (rowView == null) {
                LayoutInflater inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
                rowView = inflater.inflate(R.layout.list_item_transaction, parent, false);
            }


            TextView tvAmountInAccountCurrency = (TextView) rowView.findViewById(R.id.tv_amountInAccountCurrency);
            tvAmountInAccountCurrency
                    .setText(Integer.toString(originalData.get(position).getAmountInAccountCurrency()));

            ImageView ivDirection = (ImageView) rowView.findViewById(R.id.iv_direction);
                if(originalData.get(position).getDirection().equals(Constants.Global.VAL_DIRECTION_ICOMING))
                    ivDirection.setImageResource(R.drawable.arrow_left);
                if(originalData.get(position).getDirection().equals(Constants.Global.VAL_DIRECTION_OUTGOING))
                    ivDirection.setImageResource(R.drawable.ic_arrow_right);

            TextView tvDirection = (TextView) rowView.findViewById(R.id.tv_direction);
            tvDirection
                    .setText(originalData.get(position).getDirection());

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    redirect(originalData.get(position));
                }
            });

        } catch (Exception e) {
            Logger.e(e.getMessage());
        }
        return rowView;
    }

    private void redirect(Transaction transaction) {
        Intent intent = new Intent(activity, TransactionDetailActivity.class);
        /*
        PendingIntent pendingIntent =
                TaskStackBuilder.create(this)
                        // add all of DetailsActivity's parents to the stack,
                        // followed by DetailsActivity itself
                        .addNextIntentWithParentStack(intent)
                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                        */
        /*
            Using the Parcelable is better choice, but we passing only 3 values
        */
        intent.putExtra(Constants.Global.KEY_ID, transaction.getId());
        intent.putExtra(Constants.Global.KEY_AMOUNT_IN_ACCOUNT_CURRENCY, transaction.getAmountInAccountCurrency());
        intent.putExtra(Constants.Global.KEY_DIRECTION, transaction.getDirection());
        activity.startActivity(intent);
    }
    @Override
    public Filter getFilter() {
        return transactionTypeFilter;
    }


    public class TransactionTypeFilter extends Filter {

        @SuppressWarnings("unchecked")
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filteredResults = new FilterResults();
            ArrayList<Transaction> tempList = new ArrayList<Transaction>();

            if (constraint.equals("")) {
                filteredResults.values = originalData;
                filteredResults.count = originalData.size();
            } else if (constraint.length() > 0) {
                // search in values
                for (Transaction transaction : originalData) {
                    Logger.d(transaction.getDirection());
                    if (transaction.getDirection().toLowerCase().equals(constraint.toString().toLowerCase())) {
                        tempList.add(transaction);
                    }
                }

                filteredResults.values = tempList;
                filteredResults.count = tempList.size();
            }
            return filteredResults;
        }

        /**
         * Notify about filtered list to ui
         *
         * @param constraint text
         * @param results    filtered result
         */
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Transaction>) results.values;
            notifyDataSetChanged();
        }
    }

}



